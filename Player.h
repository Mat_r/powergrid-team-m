#pragma once
#include <String>;
#include <Stack>;
#include <Vector>;
using namespace std;

#include "Resource.h"
#include "Card.h"
#include "Track.h"

class Player
{
private:
	string name;
	double money;
	string color;

	vector<Card> cards;

	//stacks of resources, max depends on the owned powerplants holding capacity
	stack<Oil> oilStack;
	int oilMax;

	stack<Coal> coalStack;
	int coalMax;

	stack<Garbage> garbageStack;
	int garbageMax;

	stack<Uranium> uraniumStack;
	int uraniumMax;


public:
	Player();
	void setPlayer(string, double, string);

	string getName();
	int getMoney();
	string getColor();
	int getOilCount();
	int getCoalCount();
	int getGarbageCount();
	int getUraniumCount();

	bool buyOil(OilTrack&);
	bool buyCoal(CoalTrack&);
	bool buyGarbage(GarbageTrack&);
	bool buyUranium(UraniumTrack&);


	~Player();


	
};

