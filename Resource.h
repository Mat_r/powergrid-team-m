#pragma once
#include <iostream>
using namespace std;


class Resource {
protected:
	string name;
public:
	Resource();
	~Resource();
	
};

class Oil : public Resource {
public:
	Oil() {
		name = "Oil";
	}
};

class Coal : public Resource {
public:
	Coal() {
		name = "Coal";
	}
};

class Garbage : public Resource {
public:
	Garbage() {
		name = "Garbage";
	}
};

class Uranium : public Resource {
public:
	Uranium() {
		name = "Uranium";
	}
};

