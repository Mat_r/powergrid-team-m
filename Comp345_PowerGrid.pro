#-------------------------------------------------
#
# Project created by QtCreator 2016-02-22T19:06:47
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Comp345_PowerGrid
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Resource.cpp \
    newGame.cpp \
    Player.cpp \
    stdafx.cpp \
    Track.cpp \
    sqlite3.c \
    view.cpp \
    Card.cpp

HEADERS  += mainwindow.h \
    newGame.h \
    Player.h \
    Resource.h \
    sqlite3.h \
    stdafx.h \
    Track.h \
    view.h \
    Card.h

FORMS    += mainwindow.ui

RESOURCES += \
    Resources/images.qrc
