#include "stdafx.h"
#include "Track.h"

/****************************************************
/				BASE TRACK FUNCTIONS				/
****************************************************/
Track::Track()
{
}

bool Track::isEmpty()
{
	if (track.size() > 0)
		return false;
	else
		return true;
}

void Track::removeRes()
{
	track.pop();
}



/****************************************************
/			RESOURCE TRACK FUNCTIONS				/
****************************************************/

/*****************************  Oil Track Functions*/
bool OilTrack::isFull()
{
	if (track.size() >= 24)
		return true;
	else
		return false;
}
int OilTrack::checkNextResVal()
{
	return coalOilGarbCheckNextResVal();
}
void OilTrack::addOil()
{
	track.push(Oil());
}
/***************************************************/

/****************************  Coal Track Functions*/
bool CoalTrack::isFull()
{
	if (track.size() >= 24)
		return true;
	else
		return false;
}
void CoalTrack::addCoal()
{
	track.push(Coal());
}
int CoalTrack::checkNextResVal()
{
	return coalOilGarbCheckNextResVal();
}
/***************************************************/

/*************************  Garbage Track Functions*/
bool GarbageTrack::isFull()
{
	if (track.size() >= 24)
		return true;
	else
		return false;
}
void GarbageTrack::addGarbage()
{
	track.push(Garbage());
}
int GarbageTrack::checkNextResVal()
{
	return coalOilGarbCheckNextResVal();
}
/***************************************************/

/*************************  Uranium Track Functions*/
bool UraniumTrack::isFull()
{
	if (track.size() >= 12)
		return true;
	else
		return false;
}
void UraniumTrack::addUranium()
{
	track.push(Uranium());
}
int UraniumTrack::checkNextResVal()
{
	switch (track.size())
	{
	case 12:
		return 1;
	case 11:
		return 2;
	case 10:
		return 3;
	case 9:
		return 4;
	case 8:
		return 5;
	case 7:
		return 6;
	case 6:
		return 7;
	case 5:
		return 8;
	case 4:
		return 10;
	case 3:
		return 12;
	case 2:
		return 14;
	case 1:
		return 16;
	}
}
/***************************************************/

/*******************  Multi-Purpose Track Functions*/
int Track::coalOilGarbCheckNextResVal()
{
	if (track.size() >= 22)
		return 1;
	else if (track.size() >= 19)
		return 2;
	else if (track.size() >= 16)
		return 3;
	else if (track.size() >= 13)
		return 4;
	else if (track.size() >= 10)
		return 5;
	else if (track.size() >= 7)
		return 6;
	else if (track.size() >= 4)
		return 7;
	else
		return 8;
	
}
