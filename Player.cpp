#include "stdafx.h"
#include "Player.h"


Player::Player()
{
}

void Player::setPlayer(string n, double m, string c)
{
	name = n;
	money = m;
	color = c;
}

string Player::getName()
{
	return name;
}
int Player::getMoney()
{
	return money;
}
string Player::getColor()
{
	return color;
}
int Player::getOilCount()
{
	return oilStack.size();
}
int Player::getCoalCount()
{
	return coalStack.size();
}
int Player::getGarbageCount()
{
	return garbageStack.size();
}
int Player::getUraniumCount()
{
	return uraniumStack.size();
}

bool Player::buyOil(OilTrack &OT)
{
	if (money >= OT.checkNextResVal() && !OT.isEmpty())
	{
		money = money - OT.checkNextResVal();
		oilStack.push(Oil());
		OT.removeRes();
		return true;
	}
	else
		return false;
	
}
bool Player::buyCoal(CoalTrack &CT)
{
	if (money >= CT.checkNextResVal() && !CT.isEmpty())
	{
		money = money - CT.checkNextResVal();
		coalStack.push(Coal());
		CT.removeRes();
		return true;
	}
	else
		return false;
}
bool Player::buyGarbage(GarbageTrack &GT)
{
	if (money >= GT.checkNextResVal() && !GT.isEmpty())
	{
		money = money - GT.checkNextResVal();
		garbageStack.push(Garbage());
		GT.removeRes();
		return true;
	}
	else
		return false;
}
bool Player::buyUranium(UraniumTrack &UT)
{
	if (money >= UT.checkNextResVal() && !UT.isEmpty())
	{
		money = money - UT.checkNextResVal();
		uraniumStack.push(Uranium());
		UT.removeRes();
		return true;
	}
	else
		return false;
}

Player::~Player()
{
}
