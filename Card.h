#pragma once

#include <iostream>
#include <String>
using namespace std;

class Card
{
		

public:
	enum cardType { Coal, Oil, Garbage, Uranium, Hybrid, Free, Step3 };

	Card(cardType, int, int, int);
	Card(cardType);

	string getType();
	int getValue();
	int getResCost();
	int getResHold();
	int getOutput();

	~Card();

private:
	//types of powerplants: 
	//oil, coal, garbage, uranium, hybrid, free

	cardType type;
	//string type;
	int value;
	int resCost;
	int output;
	int maxResHold;
};

