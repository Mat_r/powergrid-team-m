// Comp345PowerGrid.cpp : Defines the entry point for the console application.
#ifndef UNICODE
#define UNICODE
#endif

#include "mainwindow.h"
#include <QApplication>

#include "stdafx.h"
#include <iostream>
//#include "sqlite3.h"
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

//#include "newGame.h"
#include "Player.h"
#include "Track.h"
#include "Resource.h"

int main(int argc, char *argv[])
{
    /****************************************************
    /			Setup	tests							/
    ****************************************************/

    Q_INIT_RESOURCE(images);


    Player player1 = Player();
    Player player2 = Player();

    player1.setPlayer("Mat", 1000.00, "Red");
    player2.setPlayer("Sam", 1000.00, "Blue");

    OilTrack OT;

    OT.addOil();
    OT.addOil();

    //testing
    if (!OT.isFull())
    {
        cout << "not full oil track" << endl;
    }
    cout << OT.getSize() << endl;

    /****************************************************
    /			START UI OPERATIONS						/
    ****************************************************/



    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    /****************************************************
    /			START DATABASE OPERATIONS				/
    ****************************************************/


    return a.exec();
}
