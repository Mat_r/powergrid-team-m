#pragma once

#include <String>
#include <iostream>
#include "sqlite3.h"

#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

using namespace std;

class newGame
{

	sqlite3 *db;
	int rc;
	char *zErrMsg = 0;

    QSqlDatabase dbo;
	

public:
	newGame();
	~newGame();

	void createTable(char*);

	char *CT_Players();
	char *CT_PowerPlants();
	char *CT_Regions();
	char *CT_Cities();
	char *CT_Connections();

	void setupEuropeDB();

    void closeDatabase();
};

