#include "stdafx.h"
#include "newGame.h"

#include <QtWidgets>


#include <iostream>
using namespace std;

newGame::newGame()
{
}
newGame::~newGame()
{
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	int i;
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

void newGame::setupEuropeDB()
{
	
    cout << "in setup euro" << endl;


        if(!dbo.open())
        {
            cout << "opening database" << endl;
            dbo = QSqlDatabase::addDatabase("QSQLITE");
            dbo.setHostName("PowerGrid");
            dbo.setDatabaseName("EuroGame2");
            dbo.setUserName("user1");
            bool ok = dbo.open();
        }


        QSqlQuery query;
        query.exec("show tables;");


        if(!query.exec())
           qDebug() << "> Query exec() error." << query.lastError();
        else
           qDebug() << ">Query exec() success.";

        while (query.next()) {
                QString name = query.value(0).toString();
                QString salary = query.value(1).toString();
                qDebug() << name << salary;
            }


        qDebug() << dbo.tables();

        cout << "creating tables" << endl;
        query.exec(CT_Players());
        query.exec(CT_PowerPlants());
        query.exec(CT_Regions());
        query.exec(CT_Cities());
        query.exec(CT_Connections());


        //query.exec("insert into regions (id, name) values (0, 'testing123'), (1, 'yolo');");

        //query.exec("insert into regions (id, name) values (3, 'yooooooooooo'), (4, 'another test yo');");

        //query.exec("insert into regions (id, name) values (0, 'heyyyyy');");




        if(!query.exec())
           qDebug() << "> Query exec() error." << query.lastError();
        else
           qDebug() << ">Query exec() success.";

        query.exec("select * from regions;");

        while (query.next()) {
                int id = query.value(0).toInt();
                QString name = query.value(1).toString();
                qDebug() << id << name;
            }


    //cout << dbo.databaseName() << endl;

    /*rc = sqlite3_open("test.db", &db);

	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}
	else {
		fprintf(stderr, "Opened database successfully, status: %s\n", sqlite3_errmsg(db));

		//if new game
		createTable(CT_Players());
		createTable(CT_PowerPlants());
		createTable(CT_Regions());
		createTable(CT_Cities());
		createTable(CT_Connections());

	}
    sqlite3_close(db);*/

}

void newGame::createTable(char* newTable)
{
	rc = sqlite3_exec(db, newTable, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Table created successfully\n");
	}
}

    //not used yet
void newGame::closeDatabase ()
{
    QString connectionName = dbo.connectionName();
    dbo.close();
    dbo = QSqlDatabase();
    QSqlDatabase::removeDatabase(connectionName);
}

char* newGame::CT_Players()
{
	return "CREATE TABLE players "
		"(id INT PRIMARY KEY NOT NULL, "
		"name VARCHAR(128) NOT NULL, "
		"color VARCHAR(128) NOT NULL, "
		"money INT NOT NULL, "
		"oil INT NOT NULL, "
		"coal INT NOT NULL, "
		"garbage INT NOT NULL, "
		"uranium INT NOT NULL"
        "); ";
}

char* newGame::CT_PowerPlants()
{
	return "CREATE TABLE power_plants "
		"(id INT PRIMARY KEY NOT NULL, "
		"value INT NULL, "
		"owner INT NULL, "
		"FOREIGN KEY(owner) REFERENCES players(id)"
        "); ";
}

char* newGame::CT_Regions()
{
	return "CREATE TABLE regions "
		"(id INT PRIMARY KEY NOT NULL, "
		"name VARCHAR(128) NOT NULL"
        "); ";
}

char* newGame::CT_Cities()
{
	return "CREATE TABLE cities "
		"(id INT PRIMARY KEY NOT NULL, "
		"name VARCHAR(128) NOT NULL, "
		"region INT NOT NULL, "
		"owner INT NULL, "
		"FOREIGN KEY(region) REFERENCES regions(id), "
		"FOREIGN KEY(owner) REFERENCES players(id)"
        "); ";
}

char* newGame::CT_Connections()
{
	return "CREATE TABLE connections "
		"(cityID_1 INT NOT NULL, "
		"cityID_2 INT NOT NULL, "
		"cost INT NOT NULL, "
		"FOREIGN KEY(cityID_1) REFERENCES cities(id), "
		"FOREIGN KEY(cityID_2) REFERENCES cities(id)"
        "); ";
}

