#pragma once
#include <iostream>
#include <stack>
#include "Resource.h"
using namespace std;

class Track {
protected:
	
	stack <Resource> track;
	int coalOilGarbCheckNextResVal();



private:
	

public:

	Track();

	bool isEmpty();

	void removeRes();

	//***** testing fuctions *****//
	int getSize()
	{
		return track.size();
	}
};

class OilTrack : public Track {
public:
	bool isFull();
	int  checkNextResVal();
	void addOil();
};

class CoalTrack : public Track {
public:
	bool isFull();
	int  checkNextResVal();
	void addCoal();
};

class GarbageTrack : public Track {
public:
	bool isFull();
	int  checkNextResVal();
	void addGarbage();
};

class UraniumTrack : public Track {
public:
	bool isFull();
	int  checkNextResVal();
	void addUranium();
};

class ScoringTrack : public Track {
public:
};

class ProgressTrack : public Track {
public:
};

