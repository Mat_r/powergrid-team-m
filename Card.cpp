#include "stdafx.h"
#include <String>
#include "Card.h"


Card::Card(cardType t, int val, int rc, int out)
{
	type = t;
	value = val;
	resCost = rc;
	maxResHold = rc*2;
	output = out;
}

Card::Card(cardType t)
{
	type = t;
	value = 0;
}

string Card::getType()
{
	switch (type)
	{
	case Oil :
		return "Oil";
	case Coal:
		return "Coal";
	case Garbage:
		return "Garbage";
	case Uranium:
		return "Uranium";
	case Hybrid:
		return "Hybrid";
	case Free:
		return "Free";
	}
}
int Card::getValue()
{
	return value;
}
int Card::getResCost()
{
	return resCost;
}
int Card::getResHold()
{
	return maxResHold;
}
int Card::getOutput()
{
	return output;
}

Card::~Card()
{
}
