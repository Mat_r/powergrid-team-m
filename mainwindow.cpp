#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "view.h"

#include <QHBoxLayout>
#include <QSplitter>

#include <iostream>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    //ui->graphicsView->setScene(scene);

        //setup menus and actions
    createActions();
    createMenus();
    createStatusBar();
    setUnifiedTitleAndToolBarOnMac(true);


        //test brush drawing
    QBrush redBrush(Qt::red);
    QPen bluePen(Qt::blue);
    bluePen.setWidth(6);
    rectangle = scene->addRect(QRectF(0, 0, 100, 100));
    ellipse = scene->addEllipse(10,10,100,100,bluePen,redBrush);

        //add board background
    QImage im(":/france-map.jpg");
    std::cout << "hello" << endl;
    scene->addPixmap(QPixmap::fromImage(im));

        //splitters, mapview
    h1Splitter = new QSplitter;

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Vertical);
    vSplitter->addWidget(h1Splitter);

    View *view = new View("Top left view");
    view->view()->setScene(scene);
    h1Splitter->addWidget(view);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(vSplitter);
    setLayout(layout);



    ui->graphicsView->setViewport(view);



    populateBoard();
}

void MainWindow::newGame()
//! [5] //! [6]
{

    game.setupEuropeDB();



    //create new game
    /*if (maybeSave()) {
        textEdit->clear();
        setCurrentFile("");
    }*/
}
//! [6]

//! [7]
void MainWindow::open()
//! [7] //! [8]
{
    /*if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this);
        if (!fileName.isEmpty())
            loadFile(fileName);
    }*/
}
//! [8]

//! [9]
bool MainWindow::save()
//! [9] //! [10]
{
    /*if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }*/
}
//! [10]

//! [11]
bool MainWindow::saveAs()
//! [11] //! [12]
{
    /*QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList files;
    if (dialog.exec())
        files = dialog.selectedFiles();
    else
        return false;

    return saveFile(files.at(0));*/
}
//! [12]

//! [13]
void MainWindow::about()
//! [13] //! [14]
{
   QMessageBox::StandardButton ret;
   QMessageBox::about(this, tr("About Application"),
            tr("The <b>Application</b> example demonstrates how to "
               "write modern GUI applications using Qt, with a menu bar, "
               "toolbars, and a status bar."));
}

void MainWindow::createActions()
{
    newAct = new QAction(QIcon(":/images/new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new Game"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newGame()));

    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing Game"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

}

void MainWindow::createMenus()
//! [25] //! [27]
{
    fileMenu = menuBar()->addMenu(tr("&Game"));
    fileMenu->addAction(newAct);
//! [28]
    fileMenu->addAction(openAct);
//! [28]
    fileMenu->addAction(saveAct);
//! [26]
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

void MainWindow::createStatusBar()
//! [32] //! [33]
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::loadGame(const QString &fileName)
//! [42] //! [43]
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QTextStream in(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    //textEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    //setCurrentFile(fileName);
    statusBar()->showMessage(tr("File loaded"), 2000);
}
//! [43]

//! [44]
bool MainWindow::saveGame(const QString &fileName)
//! [44] //! [45]
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    //out << textEdit->toPlainText();
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    //setCurrentFile(fileName);
    statusBar()->showMessage(tr("Game saved"), 2000);
    return true;
}

QString MainWindow::strippedName(const QString &fullFileName)
//! [48] //! [49]
{
    return QFileInfo(fullFileName).fileName();
}

void MainWindow::populateBoard()
{
    //scene = new QGraphicsScene;



    //QGraphicsPixmapItem item( QPixmap::fromImage(im));
    //scene->addItem(&item);


    //QImage myImg = new QImage;
    //myImg->load(":/france-map.jpg");

    //QPixmap pix;
    //QGraphicsPixmapItem *myI = new QGraphicsPixmapItem(0, &scene);

    //myI->setPixmap(pix.fromImage( *im, Qt::AutoColor ));

    //


    //scene.addItem(im);
    //im->setPos(50, 50);
    //im->setZValue(50);

    //QPixmap pim(":/france-map.jpg");
    //scene->setBackgroundBrush(QPixmap (":/france-map.jpg"));
    ///scene->drawBackground(im, rectagle);
    //scene->setBackgroundBrush(im);
    //scene->setBackgroundBrush(pim.scaled(500, 500, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));

    //scene->addItem(image);
    // Populate board with all cities and connections (+houses if loaded game)
    /*int xx = 0;
    int nitems = 0;
    for (int i = -11000; i < 11000; i += 110) {
        ++xx;
        int yy = 0;
        for (int j = -7000; j < 7000; j += 70) {
            ++yy;
            qreal x = (i + 11000) / 22000.0;
            qreal y = (j + 7000) / 14000.0;

            QColor color(image.pixel(int(image.width() * x), int(image.height() * y)));
            QGraphicsItem *item = new Chip(color, xx, yy);
            item->setPos(QPointF(i, j));
            scene->addItem(item);

            ++nitems;
        }
    }*/
}


MainWindow::~MainWindow()
{
    delete ui;
}
